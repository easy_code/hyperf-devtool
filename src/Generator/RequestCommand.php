<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/06 13:46,
 * @LastEditTime: 2022/10/06 13:46
 */
declare(strict_types=1);

namespace Zhen\HyperfDevtool\Generator;

use Hyperf\Command\Annotation\Command;

#[Command]

class RequestCommand extends GeneratorCommand
{
    public function __construct()
    {
        parent::__construct('ext-gen:request');
        $this->setDescription('Create a new form request class');
    }

    protected function getStub(): string
    {
        return class_exists(\Zhen\HyperfKit\CoreFormRequest::class)
            ?  __DIR__ . '/stubs/validation-request-core.stub'
            : __DIR__ . '/stubs/validation-request.stub';
    }

    protected function getDefaultNamespace(): string
    {
        return 'App\\' . $this->getModuleInput() . '\\Request';
    }
}