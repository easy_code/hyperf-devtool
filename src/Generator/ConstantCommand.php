<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/06 13:46,
 * @LastEditTime: 2022/10/06 13:46
 */
declare(strict_types=1);

namespace Zhen\HyperfDevtool\Generator;

use Hyperf\Command\Annotation\Command;

#[Command]

class ConstantCommand extends GeneratorCommand
{
    public function __construct()
    {
        parent::__construct('ext-gen:constant');
        $this->setDescription('Create a new constant class');
    }

    /**
     * Get the stub file for the generator.
     */
    protected function getStub(): string
    {
        return __DIR__ . '/stubs/constant.stub';
    }

    /**
     * Get the default namespace for the class.
     */
    protected function getDefaultNamespace(): string
    {
        return 'App\\' . $this->getModuleInput() . '\\Constants';
    }
}