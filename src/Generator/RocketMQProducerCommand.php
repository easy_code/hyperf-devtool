<?php
/**
 * @Author: laoweizhen <1149243551@qq.com>,
 * @Date: 2022/10/08 9:58,
 * @LastEditTime: 2022/10/08 9:58
 */
declare(strict_types=1);

namespace Zhen\HyperfDevtool\Generator;

use Hyperf\Command\Annotation\Command;

#[Command]

class RocketMQProducerCommand extends GeneratorCommand
{
    public function __construct()
    {
        parent::__construct('ext-gen:rocketmq-producer');
        $this->setDescription('Create a new rocketmq producer class');
    }

    /**
     * Get the stub file for the generator.
     */
    protected function getStub(): string
    {
        return __DIR__ . '/stubs/rocketmq-producer.stub';
    }

    /**
     * Get the default namespace for the class.
     */
    protected function getDefaultNamespace(): string
    {
        return 'App\\' . $this->getModuleInput() . '\\Queue\\Producer';
    }
}